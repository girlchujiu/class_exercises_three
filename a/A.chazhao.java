import java.util.Scanner;
public class A.chazhao{
    public static int binarySearch(int[] a, int x) {
        int low = 0;   
        int high = a.length-1;   
        while(low <= high) {   
            int middle = (low + high)/2; 
            
            if(x == a[middle]) {   
                return middle;   
            }else if(x <a[middle]) {   
                high = middle - 1;   
            }else {   
                low = middle + 1;   
            }  
        }  
		return high;
   }

    public static void main(String[] args) {
        int[] a = {1,2,3,4,5,6,7,8,9,10 };
        Scanner reader = new Scanner(System.in);
        int number = reader.nextInt();
        if(number < a[0] || number > a[a.length-1]) {
        	System.out.println("not found");
        }
        else{
        	System.out.println("weizhi:" + (binarySearch(a, number) ));
        }
       
    }
}